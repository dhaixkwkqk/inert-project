export const RBACActions = Object.freeze({
    viewAny: "viewAny",
    view: "view",
    create: "create",
    update: "update",
    delete: "delete",
})
