import { onMounted } from 'vue';

export function makeBreakpointClass(prefix = '', value, def = '') {
    let result = '';
    if (typeof value === 'string' || typeof value === 'number') {
        return `${prefix}-${value}`;
    }
    else if (Array.isArray(value)) {
        let classArray = [];
        for (let key of value) {
            classArray.push(`${prefix}-${key}`);
        }
        result = classArray.join(' ');
    }
    else if (typeof value === 'object' && value !== null) {
        let classArray = [];
        for (const [key, val] of Object.entries(value)) {
            if (key === 'xs') {
                classArray.push(`${prefix}-${val}`);
            }
            else {
                classArray.push(`${prefix}-${key}-${val}`);
            }
        }
        result = classArray.join(' ');
    }

    return result.length === 0 ? def : result;
}

export function attributesNameId(name, id) {
    if (name && id === undefined) {
        id = name;
    }
    if (id && name === undefined) {
        name = id;
    }

    return {name, id};
}

export const PopupEvents = [
    'hide',
    'hidden',
    'hide-prevent',
    'show',
    'shown',
];

/**
 * @param {$ref} elRef
 * @param emit - defineEmits()
 * @param {string} control
 */
export function usePopupEvents(elRef, emit, control) {
    onMounted(() => {
        elRef.value.addEventListener(
            'hide.bs.' + control,
            event => emit('hide', event)
        );
        elRef.value.addEventListener(
            'hidden.bs.' + control,
            event => emit('hidden', event)
        );
        elRef.value.addEventListener(
            'hidePrevent.bs.' + control,
            event => emit('hide-prevent', event)
        );
        elRef.value.addEventListener(
            'show.bs.' + control,
            event => emit('show', event)
        );
        elRef.value.addEventListener(
            'shown.bs.' + control,
            event => emit('shown', event)
        );
    });
}
