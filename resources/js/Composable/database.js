import { usePage } from '@inertiajs/vue3';
import { assign, zipObject } from 'lodash';

export function decomposeModel(model, relations = []) {
    return Object.keys(model).reduce((accumulator, currentValue) => {
        if (relations.includes(currentValue)) {
            accumulator[currentValue] = accumulator[currentValue].reduce((acc, val) => {
                acc.push(val.id);
                return acc;
            }, []);
        }
        return accumulator;
    }, model);
}

export function defaultLocalizedValue() {
    const page = usePage();

    return zipObject(page.props.availableLocales);
}

/**
 * @param {Object|null} value
 */
export function assignLocalizedValue(value) {
    return assign(defaultLocalizedValue(), value);
}
