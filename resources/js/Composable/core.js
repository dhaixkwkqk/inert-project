import { onMounted, onUnmounted, ref, toRaw } from 'vue';
import { findKey, isEmpty } from 'lodash';

export function onWindowResize(callback) {
    const doc = ref(document.documentElement);

    function proxyCallback(event) {
        doc.value = document.documentElement;
        callback(doc.value.clientWidth, doc.value.clientHeight, event);
    }

    onMounted(() => {
        window.addEventListener('resize', proxyCallback);
        callback(doc.value.clientWidth, doc.value.clientHeight);
    });
    onUnmounted(() => {
        window.removeEventListener('resize', proxyCallback);
    });
}

export function useEventListener(target, event, callback) {
    if (target) {
        onMounted(() => target.addEventListener(event, callback))
        onUnmounted(() => target.removeEventListener(event, callback))
    }
}

/**
 * @param {Array|Object} collection
 * @param {Array|Object} expression
 * @returns {undefined|Object}
 */
export function findRecursive(collection, expression) {
    let result = undefined;
    collection = toRaw(collection);
    for (let collectionElement of collection) {
        if (findKey(collectionElement,  (value, key) => value === expression[key])) {
            return collectionElement;
        }
        if (! isEmpty(collectionElement.children)) {
            result = findRecursive(collectionElement.children, expression);
            if (result)
                return result;
        }
    }

    return result;
}

