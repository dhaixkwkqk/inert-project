import './bootstrap';
import { Button, Collapse, Dropdown, Tab, Modal, Offcanvas } from 'bootstrap';
import Col from '@/Components/Col.vue';
import Row from '@/Components/Row.vue';

import { createApp, h } from 'vue';
import { createInertiaApp, usePage } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';
import i18n from '@/Plugins/i18n';
import { bootstrapDirective } from '@/Plugins/bootstrap';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        el.classList.add('container-fluid', 'min-vh-100');
        let app = createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(ZiggyVue, Ziggy)
            .use(i18n)
            .component('Col', Col)
            .component('Row', Row)
            .directive('bs', bootstrapDirective);
        /**
         * @param {string} ability
         */
        app.config.globalProperties.$can = (ability) => {
            const page = usePage();
            const permissions = page.props.auth?.permissions;
            if (permissions) {
                return Boolean(permissions.find(item => item === ability));
            }

            return false;
        }
        /**
         * @param {string[]} actions
         * @returns boolean
         */
        app.config.globalProperties.$canAny = (actions = []) => {
            const page = usePage();
            const permissions = page.props.auth?.permissions;

            return Boolean(permissions.find(element => actions.includes(element)));
        }
        /**
         * @param {object} object
         * @param {string[]} abilities
         */
        app.config.globalProperties.$canAnyObject = (object, ...abilities) => {
            const page = usePage();
            const permissions = page.props.auth?.permissions;
            if (permissions) {
                let result = false;
                let srcAbilities = abilities.map(action => `${object}-${action}`);

                abilities.forEach(item => {
                    result = Boolean(permissions.find(element => srcAbilities.includes(element)));
                    if (result) {
                        return result;
                    }
                });
                return result;
            }

            return false;
        }

        return app.mount(el);
    },
    progress: {
        color: '#FF2D20',
    },
});
