class BootstrapDirective {
    static row(el, binding) {
        console.log(el, binding);
    }
}

// binding: an object containing the following properties.
//
//     value:     The value passed to the directive. For example in v-my-directive="1 + 1", the value would be 2.
//     oldValue:  The previous value, only available in beforeUpdate and updated. It is available whether or not the value has changed.
//     arg:       The argument passed to the directive, if any. For example in v-my-directive:foo, the arg would be "foo".
//     modifiers: An object containing modifiers, if any. For example in v-my-directive.foo.bar, the modifiers object would be { foo: true, bar: true }.
//     instance:  The instance of the component where the directive is used.
//     dir:       the directive definition object.

export const bootstrapDirective = {
    // called before bound element's attributes
    // or event listeners are applied
    created(el, binding, vnode, prevVnode) {
        // see below for details on arguments
    },
    // called right before the element is inserted into the DOM.
    beforeMount(el, binding, vnode, prevVnode) {
        try {
            BootstrapDirective[binding.arg](el, binding);
        }
        catch (e) {
            console.debug(e.toString());
        }
    },
    // called when the bound element's parent component
    // and all its children are mounted.
    mounted(el, binding, vnode, prevVnode) {},
    // called before the parent component is updated
    beforeUpdate(el, binding, vnode, prevVnode) {},
    // called after the parent component and
    // all of its children have updated
    updated(el, binding, vnode, prevVnode) {},
    // called before the parent component is unmounted
    beforeUnmount(el, binding, vnode, prevVnode) {},
    // called when the parent component is unmounted
    unmounted(el, binding, vnode, prevVnode) {},
}
