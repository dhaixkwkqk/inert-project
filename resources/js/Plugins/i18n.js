import { createI18n } from 'vue-i18n';
import en from '../../../lang/en.json';
import ru from '../../../lang/ru.json';

export default {
    install: async (app, options) => {
        const i18n = createI18n({
            legacy: false,
            locale: 'ru',
            fallbackLocale: 'en',
            missingWarn: false,
            fallbackWarn: false,
            silentTranslationWarn: true,
            messages: { ru, en }
        });
        app.use(i18n);
    }
}
