export function normalizeModel(model, relations = []) {
    return Object.keys(model).reduce((accumulator, currentValue) => {
        if (relations.includes(currentValue)) {
            accumulator[currentValue] = accumulator[currentValue].reduce((acc, role) => {
                acc.push(role.id);
                return acc;
            }, []);
        }
        return accumulator;
    }, model);
}
