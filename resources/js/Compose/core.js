import { onMounted, onUnmounted } from 'vue';

export function onWindowResize(callback) {
    function proxyCallback(event) {
        callback(window.innerWidth, window.innerHeight, event);
    }

    onMounted(() => window.addEventListener('resize', proxyCallback));
    onUnmounted(() => window.removeEventListener('resize', proxyCallback));
}
