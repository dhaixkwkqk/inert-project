<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AppController::class, 'index'])
    ->name('index');
Route::post('/locale', [AppController::class, 'locale'])
    ->name('locale');

Route::get('/dashboard', [AppController::class, 'dashboard'])
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])
        ->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])
        ->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])
        ->name('profile.destroy');

    Route::prefix('admin')->name('admin.')->group(function () {
        Route::get('/', [AdminController::class, 'index'])
            ->name('index');
        Route::get('routes', [AdminController::class, 'routes'])
            ->name('routes');

        Route::resources([
            'users'       => UserController::class,
            'roles'       => RoleController::class,
            'permissions' => PermissionController::class,
        ], [
            'except' => ['show']
        ]);
        Route::post('/users/destroy-many', [UserController::class, 'destroyMany'])
            ->name('users.destroy-many');
        Route::post('/roles/destroy-many', [RoleController::class, 'destroyMany'])
            ->name('roles.destroy-many');
        Route::post('/permissions/destroy-many', [PermissionController::class, 'destroyMany'])
            ->name('permissions.destroy-many');
        // Menu
        Route::controller(MenuController::class)->prefix('menus')->name('menus.')->group(function () {
            Route::get('', 'index')
                ->name('index');
            Route::put('/{menu?}', 'save')
                ->name('save');
            Route::delete('/destroy/{menu?}', 'destroy')
                ->name('destroy');
        });
    });
});

require __DIR__ . '/auth.php';
