<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id')->nullable()->constrained('menus')->cascadeOnDelete();
            $table->integer('order')->default(0);
            $table->string('type', 50);
            $table->string('position', 50)->nullable();
            $table->jsonb('name');
            $table->jsonb('description')->nullable();
            $table->bigInteger('source_id')->nullable();
            $table->string('url', 500)->nullable();
            $table->string('target', 7)->nullable();

            $table->index(['order', 'position']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menus');
    }
};
