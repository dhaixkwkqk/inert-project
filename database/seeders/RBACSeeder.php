<?php

namespace Database\Seeders;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Enums\Authorization\Roles;
use App\Models\Ability;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RBACSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /**
         * Abilities
         */
        DB::table('abilities')->delete();
        $crudActions = [Actions::viewAny, Actions::create, Actions::view, Actions::update, Actions::delete];

        /** @var Ability $ability */
        Ability::create([
            'object' => Objects::management,
            'actions' => [Actions::view]
        ]);

        // System
        /** @var Ability $ability */
        $ability = Ability::create([
            'order' => 1,
            'object' => Objects::system,
            'actions' => [Actions::view]
        ]);
        $ability->children()->createMany([
            [
                'object' => Objects::route,
                'actions' => [Actions::view]
            ],
            [
                'order' => 1,
                'object' => Objects::setting,
                'actions' => $crudActions
            ],
        ]);

        // Security
        /** @var Ability $ability */
        $ability = Ability::create([
            'order' => 2,
            'object' => Objects::security,
            'actions' => [Actions::view]
        ]);
        $ability->children()->createMany([
            [
                'object' => Objects::profile,
                'actions' => $crudActions
            ],
            [
                'order' => 1,
                'object' => Objects::user,
                'actions' => $crudActions
            ],
            [
                'order' => 2,
                'object' => Objects::role,
                'actions' => $crudActions
            ],
            [
                'order' => 3,
                'object' => Objects::permission,
                'actions' => $crudActions
            ],
        ]);

        // Menus
        /** @var Ability $ability */
        $ability->children()->createMany([
            [
                'object' => Objects::menu,
                'actions' => $crudActions
            ],
        ]);

        // Content
        /** @var Ability $ability */
        $ability = Ability::create([
            'order' => 4,
            'object' => Objects::content,
            'actions' => [Actions::view]
        ]);
        $ability->children()->createMany([
            [
                'object' => Objects::article,
                'actions' => $crudActions
            ],
        ]);

        /**
         * RBAC
         */

        // Super User
        /** @var Role $role */
        $role = Role::updateOrCreate(
            ['system' => true, 'name' => Roles::SuperUser->name],
            ['description' => 'System super user']
        );

        $permissions = [];
        Ability::each(function ($model, $key) use (&$permissions) {
            foreach ($model->actions as $action) {
                $permissions[] = Permission::updateOrCreate(
                    ['object' => $model->object->name, 'action' => $action->name],
                    ['description' => Str::ucfirst($model->object->name).' ability '.$action->name]
                )->id;
            }
        });
        $role->permissions()->sync($permissions);


        /** @var User $user */
        $user = User::updateOrCreate(
            ['email' => 'admin@domain.com'],
            [
                'name'              => fake()->name(),
                'email_verified_at' => now(),
                'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token'    => Str::random(10),
            ]
        );
        $user->roles()->sync($role->id);

        // Authenticated
        $role = Role::updateOrCreate(
            ['system' => true, 'name' => Roles::Authenticated->name],
            ['description' => 'Authenticated user']
        );

        $permissions = Permission::where('object', Objects::profile->name)->pluck('id')->toArray();
        $role->permissions()->sync($permissions);
    }
}
