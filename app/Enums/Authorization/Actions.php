<?php

namespace App\Enums\Authorization;

use App\Enums\EnumToArray;

enum Actions
{
    use EnumToArray;

    case viewAny;
    case view;
    case create;
    case update;
    case delete;
}
