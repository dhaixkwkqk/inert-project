<?php

namespace App\Enums\Authorization;

use App\Enums\EnumToArray;

enum Objects
{
    use EnumToArray;

    case management;
    // Security
    case security;
    case profile;
    case user;
    case role;
    case permission;
    // System
    case system;
    case route;
    case setting;
    // Menus
    case menu;
    // Content
    case content;
    case article;
}
