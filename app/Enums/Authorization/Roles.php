<?php

namespace App\Enums\Authorization;

use App\Enums\EnumToArray;

enum Roles
{
    use EnumToArray;

    case Authenticated;
    case SuperUser;
}
