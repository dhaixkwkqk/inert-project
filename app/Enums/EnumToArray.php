<?php

namespace App\Enums;

trait EnumToArray
{
    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function array(): array
    {
        if (is_subclass_of(self::class, \BackedEnum::class)) {
            return array_combine(self::names(), self::values());
        }
        else
            return self::names();
    }

    public static function assoc(): array
    {
        if (is_subclass_of(self::class, \BackedEnum::class)) {
            return array_combine(self::names(), self::values());
        }
        else
            return array_combine(self::names(), self::names());
    }
}
