<?php

namespace App\Enums;

enum CRUDRedirects
{
    case edit;
    case close;
    case create;
}
