<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        if (Schema::hasTable('roles') && Schema::hasTable('permissions')) {
            Permission::each(function (Permission $permission) {
                Gate::define($permission->ability, static function (User $user, Model $model = null) use ($permission) {
                    return (bool)$user->permissions
                        ->where('object', $permission->object)
                        ->where('action', $permission->action)
                        ->first();
                });
            });
        }
    }
}
