<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $ability = Rule::unique('permissions')->where(fn ($query) => $query->where([
            ['object', '=', $this->object],
            ['action', '=', $this->action]
        ]));
        if ($this->id) {
            $ability->ignore($this->id);
        }

        return [
            'object' => ['required', 'string', 'max:50', $ability],
            'action' => ['required', 'string', 'max:50', $ability],
            'description' => 'nullable|string|max:150',
        ];
    }
}
