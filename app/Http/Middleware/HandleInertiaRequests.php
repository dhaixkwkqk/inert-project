<?php

namespace App\Http\Middleware;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     */
    public function version(Request $request): string|null
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @return array<string, mixed>
     */
    public function share(Request $request): array
    {
        /** @var User $user */
        $user = $request->user();

        return array_merge(parent::share($request), [
            'auth' => [
                'user' => $user,
                'permissions' => $user?->permissions?->map(fn (Permission $permission) => $permission->ability)->all()
            ],
            'ziggy' => fn () =>
                array_merge((new Ziggy)->toArray(), [
                    'location' => $request->url(),
                ])
            ,
            'locale' => app()->getLocale(),
            'availableLocales' => config('app.available_locales'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
        ]);
    }
}
