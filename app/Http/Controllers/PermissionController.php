<?php

namespace App\Http\Controllers;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Http\Requests\PermissionRequest;
use App\Models\Permission;
use App\Services\CRUDService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::viewAny->name);

        return Inertia::render('Management/Permission/Index', [
            'permissions' => $service
                ->filter(Permission::class, $request->only(['where', 'order']))
                ->paginate($request->integer('perPage', 20), ['id', 'object', 'action', 'description'])
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::create->name);

        return Inertia::render('Management/Permission/Item');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PermissionRequest $request, CRUDService $service): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::create->name);

        $validated = $request->validated();
        $service->save(Permission::class, $validated);

        return $service->redirect('management.permissions', $request->input('_redirect'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Permission $permission)
    {
        $this->authorize(Objects::permission->name.'-'.Actions::update->name);

        return Inertia::render('Management/Permission/Item', [
            'permission' => $permission,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PermissionRequest $request, Permission $permission, CRUDService $service)
    {
        $this->authorize(Objects::permission->name.'-'.Actions::update->name);

        $validated = $request->validated();
        $service->save($permission, $validated);

        return $service->redirect('management.permissions', $request->input('_redirect'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Permission $permission): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::delete->name);

        $permission->delete();

        return to_route('management.permissions.index');
    }

    /**
     * Remove the specified resources from storage.
     */
    public function destroyMany(Request $request): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::delete->name);

        Permission::whereIn('id', $request->input('checked'))->delete();

        return to_route('management.permissions.index');
    }
}
