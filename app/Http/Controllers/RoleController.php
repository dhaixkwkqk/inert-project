<?php

namespace App\Http\Controllers;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Models\Ability;
use App\Models\Permission;
use App\Models\Role;
use App\Services\CRUDService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Inertia\Response;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response|AuthorizationException
    {
        $this->authorize(Objects::role->name.'-'.Actions::viewAny->name);

        return Inertia::render('Management/Role/Index', [
            'roles' => $service
                ->filter(Role::class, $request->only(['where', 'order']))
                ->paginate($request->integer('perPage', 20), ['id', 'name', 'system', 'description'])
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response|AuthorizationException
    {
        $this->authorize(Objects::role->name.'-'.Actions::create->name);

        return Inertia::render('Management/Role/Item', [
            'abilities' => Ability::with('children')
                ->whereNull('parent_id')
                ->orderBy('order')
                ->get(),
            'permissions' => Permission::all(['id', 'object', 'action']),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, CRUDService $service): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::role->name.'-'.Actions::create->name);

        $validated = $request->validate([
            'system' => 'boolean',
            'name' => 'required|string|max:150|unique:'.Role::class,
            'description' => 'nullable|string|max:150',
            'permissions' => 'required|array|min:1'
        ]);
        $permissions = Arr::pull($validated, 'permissions');

        $service
            ->save(Role::class, $validated)
            ->permissions()->sync($permissions);

        return $service->redirect('management.roles', $request->input('_redirect'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        $this->authorize(Objects::role->name.'-'.Actions::create->name);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        $this->authorize(Objects::role->name.'-'.Actions::update->name);

        return Inertia::render('Management/Role/Item', [
            'role' => $role->load('permissions:id'),
            'abilities' => Ability::with('children')
                ->whereNull('parent_id')
                ->orderBy('order')
                ->get(),
            'permissions' => Permission::all(['id', 'object', 'action']),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $role, CRUDService $service)
    {
        $this->authorize(Objects::role->name.'-'.Actions::update->name);

        $validated = $request->validate([
            'system' => 'boolean',
            'name' => ['required', 'string', 'max:150', Rule::unique('roles')->ignore($role)],
            'description' => 'nullable|string|max:150',
            'permissions' => 'required|array|min:1'
        ]);
        $permissions = Arr::pull($validated, 'permissions');

        $service
            ->save($role, $validated)
            ->permissions()->sync($permissions);

        return $service->redirect('management.roles', $request->input('_redirect'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::role->name.'-'.Actions::delete->name);

        if (!$role->system) {
            $role->delete();
        }

        return to_route('management.roles.index');
    }

    /**
     * Remove the specified resources from storage.
     */
    public function destroyMany(Request $request): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::role->name.'-'.Actions::delete->name);

        Role::whereIn('id', $request->input('checked'))
            ->whereNot('system', true)
            ->delete();

        return to_route('management.roles.index');
    }
}
