<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Services\CRUDService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Inertia\Response;
use function to_route;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::viewAny->name);

        return Inertia::render('Admin/User/Index', [
            'users' => $service
                ->filter(User::class, $request->only(['like', 'where', 'order']))
                ->paginate($request->integer('perPage', 20), ['id', 'name', 'email'])
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::create->name);

        return Inertia::render('Admin/User/Item', [
            'roles' => Role::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, CRUDService $service): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::create->name);

        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:'.User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'roles' => 'required|array|min:1'
        ]);
        $roles = Arr::pull($validated, 'roles');

        $service
            ->save(
                User::class,
                $validated,
                static function($data) {
                    $data['password'] = Hash::make($data['password']);
                    return $data;
                }
            )->roles()->sync($roles);

        return $service->redirect('admin.users', $request->input('_redirect'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): Response|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::update->name);

        return Inertia::render('Admin/User/Item', [
            'user' => $user->load('roles:id'),
            'roles' => Role::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user, CRUDService $service): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::update->name);

        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user)],
            'password' => ['sometimes','required', 'confirmed', Rules\Password::defaults()],
            'password_confirmation' => 'sometimes|required',
            'roles' => 'required|array|min:1'
        ]);
        $roles = Arr::pull($validated, 'roles');

        $service
            ->save(
                $user,
                $validated,
                static function($data) {
                    if (isset($data['password'])) {
                        $data['password'] = Hash::make($data['password']);
                    }
                    return $data;
                }
            )->roles()->sync($roles);

        return $service->redirect('admin.users', $request->input('_redirect'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::delete->name);

        $user->delete();

        return to_route('admin.users.index');
    }

    /**
     * Remove the specified resources from storage.
     */
    public function destroyMany(Request $request): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::user->name.'-'.Actions::delete->name);

        User::whereIn('id', $request->input('checked'))->delete();

        return to_route('admin.users.index');
    }
}
