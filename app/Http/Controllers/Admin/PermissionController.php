<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Models\Permission;
use App\Services\CRUDService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use function to_route;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::viewAny->name);

        return Inertia::render('Admin/Permission/Index', [
            'permissions' => $service
                ->filter(Permission::class, $request->only(['like', 'where', 'order']))
                ->paginate($request->integer('perPage', 20), ['id', 'object', 'action', 'description'])
                ->withQueryString(),
            'objects' => Objects::names(),
            'actions' => Actions::names(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::create->name);

        return Inertia::render('Admin/Permission/Item', [
            'objects' => Objects::names(),
            'actions' => Actions::names(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PermissionRequest $request, CRUDService $service): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::create->name);

        $validated = $request->validated();
        $service->save(Permission::class, $validated);

        return $service->redirect('admin.permissions', $request->input('_redirect'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Permission $permission)
    {
        $this->authorize(Objects::permission->name.'-'.Actions::update->name);

        return Inertia::render('Admin/Permission/Item', [
            'permission' => $permission,
            'objects' => Objects::names(),
            'actions' => Actions::names(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PermissionRequest $request, Permission $permission, CRUDService $service)
    {
        $this->authorize(Objects::permission->name.'-'.Actions::update->name);

        $validated = $request->validated();
        $service->save($permission, $validated);

        return $service->redirect('admin.permissions', $request->input('_redirect'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Permission $permission): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::delete->name);

        $permission->delete();

        return to_route('admin.permissions.index');
    }

    /**
     * Remove the specified resources from storage.
     */
    public function destroyMany(Request $request): RedirectResponse|AuthorizationException
    {
        $this->authorize(Objects::permission->name.'-'.Actions::delete->name);

        Permission::whereIn('id', $request->input('checked'))->delete();

        return to_route('admin.permissions.index');
    }
}
