<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): Response|AuthorizationException
    {
        $this->authorize(Objects::menu->name.'-'.Actions::viewAny->name);

        $items = Menu::whereNull('parent_id')
            ->orderBy('order')
            ->get()
            ->values();

        function buildTree(Collection $items): array
        {
            $result = [];
            foreach ($items as $item) {
                $children = [];
                if ($item->children->count() > 0) {
                    $children = buildTree($item->children->sortBy('order')->values());
                }
                $item = $item->toArray();
                $item['children'] = $children;

                $result[] = $item;
            }

            return $result;
        }

        return Inertia::render('Admin/Menu/Index', [
            'items' => buildTree($items)
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function save(Request $request, ?Menu $menu): RedirectResponse
    {
        $this->authorize(Objects::menu->name.'-'.Actions::update->name);

        $validated = $request->validate([
            'parent_id' => 'nullable|numeric',
            'order' => 'numeric',
            'type' => 'required|string',
            'position' => 'nullable',
            'name' => 'required|array'
        ]);

        if ($menu) {
            $menu->update($validated);
        } else {
            Menu::create($validated);
        }

        return to_route('admin.menus.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, ?Menu $menu): RedirectResponse
    {
        $this->authorize(Objects::menu->name.'-'.Actions::delete->name);

        return to_route('admin.menus.index');
    }
}
