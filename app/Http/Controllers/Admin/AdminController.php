<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Inertia\Response;

class AdminController
{
    public function index(): Response
    {
        return Inertia::render('Admin/Dashboard');
    }

    public function routes(): Response
    {
        return Inertia::render('Admin/Routes', [
            'routes' => Route::getRoutes()->getRoutes()
        ]);
    }
}
