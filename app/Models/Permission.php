<?php

namespace App\Models;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'object',
        'action',
        'description',
    ];

    protected $casts = [
        'object' => Objects::class,
        'action' => Actions::class,
    ];

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    public function ability(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->object->name.'-'.$this->action->name
        );
    }
}
