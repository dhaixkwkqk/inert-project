<?php

namespace App\Models;

use App\Enums\Authorization\Actions;
use App\Enums\Authorization\Objects;
use Illuminate\Database\Eloquent\Casts\AsEnumArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ability extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'parent_id',
        'order',
        'object',
        'actions',
    ];

    protected $casts = [
        'object' => Objects::class,
        'actions' => AsEnumArrayObject::class.':'.Actions::class,
    ];

    public function children(): HasMany
    {
        return $this->hasMany(Ability::class, 'parent_id', 'id');
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Ability::class, 'parent_id');
    }
}
