<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'groupable',
        'name',
        'description',
        'slug',
        'order'
    ];

    protected $casts = [
        'name' => 'array'
    ];
}
