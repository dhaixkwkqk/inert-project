<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Menu extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'parent_id',
        'order',
        'type',
        'position',
        'name',
        'description',
        'source_id',
        'url',
        'target',
    ];

    protected $casts = [
        'name'        => 'array',
        'description' => 'array',
    ];

    public function parent(): HasOne
    {
        return $this->hasOne(Menu::class, 'id', 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }
}
